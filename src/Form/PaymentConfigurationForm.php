<?php

namespace Drupal\chargelogic_payment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PaymentConfigurationForm.
 */
class PaymentConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'chargelogic_payment.paymentconfiguration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('chargelogic_payment.paymentconfiguration');
    $form['chargelogic_connect_store_no'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ChargeLogic Connect Store No'),
      '#description' => $this->t('Chargelogic connect store number'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('chargelogic_connect_store_no'),
    ];
    $form['chargelogic_username'] = [
        '#type' => 'textfield',
        '#title' => $this->t('ChargeLogic Connect Username'),
        '#description' => $this->t('ChargeLogic Connect Username'),
        '#maxlength' => 64,
        '#size' => 64,
        '#default_value' => $config->get('chargelogic_username'),
    ];

    $form['chargelogic_password'] = [
      '#type' => 'password',
      '#title' => $this->t('ChargeLogic Connect Password'),
      '#description' => $this->t('ChargeLogic Connect Password'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('chargelogic_password'),
    ];

    $form['chargelogic_connect_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ChargeLogic Connect Api key'),
      '#description' => $this->t('ChargeLogic connect api key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('chargelogic_connect_api_key'),
    ];
    $form['chargelogic_connect_application_'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ChargeLogic Connect Application No'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('chargelogic_connect_application_'),
    ];
    $form['chargelogic_application_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ChargeLogic Application Number'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('chargelogic_application_number'),
    ];
    $form['credit_card_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Credit Card types'),
      '#description' => $this->t('American Express is not available in the UK. Only'),
      '#options' => ['visa' => $this->t('Visa'), 'mastercard' => $this->t('Mastercard'), 'maestro' => $this->t('Maestro'), 'amex' => $this->t('American Express'), 'discover' => $this->t('Discover Card')],
      '#default_value' => $config->get('credit_card_types'),
    ];
    $form['require_the_security_code_ie_cvv'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require the security code (ie cvv) to process credit card transactions'),
      '#description' => $this->t('This should match the similar setting in your PayPal account.'),
      '#default_value' => $config->get('require_the_security_code_ie_cvv'),
    ];
    $form['default_currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Default currency'),
      '#description' => $this->t('Transactions in other currencies will be converted to this currency.'),
      '#options' => ['USD' => $this->t('USD'), 'INR' => $this->t('INR'), 'Maestro' => $this->t('Maestro'), 'American Express' => $this->t('American Express'), 'Discover Card' => $this->t('Discover Card')],
      '#size' => 5,
        '#multiple' => FALSE,
      '#default_value' => $config->get('default_currency'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('chargelogic_payment.paymentconfiguration')
        ->set('chargelogic_connect_store_no', $form_state->getValue('chargelogic_connect_store_no'))
        ->set('chargelogic_username', $form_state->getValue('chargelogic_username'))
        ->set('chargelogic_password', $form_state->getValue('chargelogic_password'))
        ->set('chargelogic_connect_api_key', $form_state->getValue('chargelogic_connect_api_key'))
        ->set('chargelogic_connect_application_', $form_state->getValue('chargelogic_connect_application_'))
        ->set('chargelogic_application_number', $form_state->getValue('chargelogic_application_number'))
        ->set('credit_card_types', $form_state->getValue('credit_card_types'))
        ->set('require_the_security_code_ie_cvv', $form_state->getValue('require_the_security_code_ie_cvv'))
        ->set('default_currency', $form_state->getValue('default_currency'))
      ->save();
  }

}
