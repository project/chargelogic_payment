<?php

namespace Drupal\chargelogic_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\chargelogic_payment\ChargeLogicConnectClient;
use Drupal\chargelogic_payment\Controller\CreateCreditCardAuthorize;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreditCardPaymentController.
 */
class CreditCardPaymentController extends ControllerBase {

  /**
   * Makepayment.
   *
   * @return string
   *   Return Hello string.
   */
  protected $configFactory;

  public function __construct(ConfigFactoryInterface $configF = NULL) {
    $this->configFactory = $configF;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('config.factory')
    );
  }


  public  function MakePayment($card = NULL, $transaction = NULL, $billingaddress = NULL) {
   // $obj = new ChargeLogicConnectClient('username', 'password');

  $config = $this->configFactory->get('chargelogic_payment.paymentconfiguration');

  $credential = array(
    "StoreNo" => $config ->get('chargelogic_connect_store_no'),
    "APIKey" => $config ->get('chargelogic_connect_api_key'),
    "ApplicationNo" => $config ->get('chargelogic_connect_application_'),
    "ApplicationVersion" => $config ->get('chargelogic_application_number')
  );

  $body = array(
    'credential' => $credential,
    'card' => $card,
    'transaction' => $transaction,
    'billingAddress' => $billingaddress,
   );

    $obj =  new ChargeLogicConnectClient($config->get('chargelogic_username'), $config->get('chargelogic_password'));
    $response = $obj->getCreditCardTransactions()->createCreditCardAuthorize($body);
    return $response;
  }

  public function MakeReversePayment($OriginalReferenceNumber = NULL, $Amount = NULL)
  {

      $config = $this->configFactory->get('chargelogic_payment.paymentconfiguration');
      $credential = array(
          "StoreNo" => $config->get('chargelogic_connect_store_no'),
          "APIKey" => $config->get('chargelogic_connect_api_key'),
          "ApplicationNo" => $config->get('chargelogic_connect_application_'),
          "ApplicationVersion" => $config->get('chargelogic_application_number')
      );

      $referenceTransaction = array(
          "OriginalReferenceNumber" => $OriginalReferenceNumber,
          "Amount" => $Amount,
      );

      $body = array(
          'credential' => $credential,
          'referenceTransaction' => $referenceTransaction,
      );
      $obj = new ChargeLogicConnectClient($config->get('chargelogic_username'), $config->get('chargelogic_password'));
      $response = $obj->getCreditCardTransactions()->createCreditCardReverse($body);
      return $response;
  }
}
