<?php
/*
 * ChargeLogicConnect
 *
 * This file was automatically generated for ChargeLogic Connect by APIMATIC v2.0 ( https://apimatic.io ).
 */

namespace Drupal\chargelogic_payment\Controller;

use Drupal\chargelogic_payment\Http\HttpCallBack;
use Drupal\chargelogic_payment\Http\HttpContext;
use Drupal\chargelogic_payment\Http\HttpResponse;
use Drupal\chargelogic_payment\APIException;
use Drupal\chargelogic_payment\Exceptions;
use \apimatic\jsonmapper\JsonMapper;
use \Unirest\Request;

/**
* Base controller
*/
class BaseControllerCustom
{
    /**
     * User-agent to be sent with API calls
     * @var string
     */
    const USER_AGENT = 'ChargeLogic-APIMATIC';

    /**
     * HttpCallBack instance associated with this controller
     * @var HttpCallBack
     */
    private $httpCallBack = null;

    /**
     * Set HttpCallBack for this controller
     * @param HttpCallBack $httpCallBack Http Callbacks called before/after each API call
     */
    public function setHttpCallBack(HttpCallBack $httpCallBack)
    {
        $this->httpCallBack = $httpCallBack;
    }

    /**
     * Get HttpCallBack for this controller
     * @return HttpCallBack The HttpCallBack object set for this controller
     */
    public function getHttpCallBack()
    {
        return $this->httpCallBack;
    }

    /**
     * Get a new JsonMapper instance for mapping objects
     * @return \apimatic\jsonmapper\JsonMapper JsonMapper instance
     */
    protected function getJsonMapper()
    {
        $mapper = new JsonMapper();
        return $mapper;
    }

    protected function validateResponse(HttpResponse $response, HttpContext $_httpContext)
    {
        //echo "<pre>"; print_r($response); die;
        if (($response->getStatusCode() < 200) || ($response->getStatusCode() > 208)) { //[200,208] = HTTP OK
            throw new APIException('HTTP Response Not OK', $_httpContext);
        }
    }
}
