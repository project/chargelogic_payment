<?php

namespace Drupal\chargelogic_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\chargelogic_payment\ChargeLogicConnectClient;
use Drupal\chargelogic_payment\Controller\CheckTransactionsController;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CheckPaymentController.
 */
class CheckPaymentController extends ControllerBase {

    /**
     * Makepayment.
     *
     * @return string
     *   Return Hello string.
     */
    protected $configFactory;

    public function __construct(ConfigFactoryInterface $configF = NULL) {
        $this->configFactory = $configF;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        // Instantiates this form class.
        return new static(
        // Load the service required to construct this class.
            $container->get('config.factory')
        );
    }

  /**
   * Checkpayment.
   *
   * @return string
   *   Return Hello string.
   */
  public function CheckPayment($demandDepositAccount = array(), $transaction = array(), $billingaddress = array(), $identification = array()) {
    $config = $this->configFactory->get('chargelogic_payment.paymentconfiguration');

    $credential = array(
        "StoreNo" => $config ->get('chargelogic_connect_store_no'),
        "APIKey" => $config ->get('chargelogic_connect_api_key'),
        "ApplicationNo" => $config ->get('chargelogic_connect_application_'),
        "ApplicationVersion" => $config ->get('chargelogic_application_number')
    );


    $body = array(
        'credential' => $credential,
        'demandDepositAccount' => $demandDepositAccount,
        'transaction' => $transaction,
        'identification' => $identification,
        'billingAddress' => $billingaddress,
    );

    $obj =  new ChargeLogicConnectClient($config ->get('chargelogic_username'), $config ->get('chargelogic_password'));
    $response = $obj->getCheckTransactions()->createCheckCharge($body);
    return $response;
    }
}
