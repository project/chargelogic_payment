<?php

namespace Drupal\chargelogic_payment\Plugin\Commerce\PaymentGateway;


use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;


/**
 * Provides the On-site checking payment gateway.
 *
 *
 * @CommercePaymentGateway(
 *   id = "chargelogic_checking",
 *   label = "Chargelogic Checking",
 *   display_label = "Chargelogic Cheking pay",
 *   forms = {
 *     "add-payment-method" = "Drupal\chargelogic_payment\PluginForm\CheckingMethodAddForm",
 *   },
 *   payment_method_types = {"chargelogic_checking_method"},
 * )
 */
class CheckingOnsite extends OnsitePaymentGatewayBase implements OnsiteInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $response = $this->makePayment($payment);


  $expires = 300;
  $next_state = 'authorization';

  if (!empty($response->ErrorMessage)) {
      \Drupal::logger('chargelogic_payment')->error($response->ErrorMessage);
      \Drupal::messenger()->addMessage($this->t('The payment has failed.').$response->ErrorMessage, 'error');
      throw new PaymentGatewayException($response->ErrorMessage);
  }

  if (!empty($response->CardVerificationValueAlert)) {
      \Drupal::logger('chargelogic_payment')->error($response->CardVerificationValueAlert);
      \Drupal::messenger()->addMessage($this->t('The payment has failed due to card mismatch').$response->CardVerificationValueAlert, 'error');
      throw new HardDeclineException($response->CardVerificationValueAlert);
  }

  if ($response->TransactionStatus == "Approved" && $capture) {
      $next_state = 'completed';
  }

  $payment->setExpiresTime($expires);
  $payment->setState($next_state);
  $remote_id = $response->return_value;
  $payment->setRemoteId($remote_id);
  $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $response = $this->makePayment($payment);
    if ($response->getStatus() == "success") {
      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();
    }
  }

  /**
   * Prepare the request and send it to the gateway.
   */
  public function makePayment(PaymentInterface $payment, $capture = TRUE) {
    $order = $payment->getOrder();
    $payment_method = $payment->getPaymentMethod();

    $billing_address = $payment_method->getBillingProfile()->get('address')->first();
    $owner = $payment_method->getOwner();
    $buyer_id = sprintf('%05d', $owner->id());

    $encryption_service = \Drupal::service('encryption');
    // See if the card is turkish or not.
    $price = $payment->getAmount()->getNumber();
    $amount = $payment->getAmount();
    $curreny_code = $payment->getAmount()->getcurrencyCode();
    $convert_to_try = FALSE;

    $demandDepositAccount = [
        "AccountNumber" => $payment_method->account_number->value,
        "RoutingNumber" => $payment_method->routing_number->value,
        "CheckType" => "Personal",
        "AccountType" => "Checking",
    ];

    $transaction = [
      "Amount" => $price,
      "ExternalReferenceNumber" => $order->id(),
      "Currency" => $curreny_code,
      "ConfirmationID" => $order->uuid(),
    ];

    $billingaddress = [
      "Name" => substr($billing_address->getAddressLine1() . ' ' . $billing_address->getAddressLine2(), 0, 60),
      "StreetAddress" => $billing_address->getAddressLine1(),
      "StreetAddress2" => $billing_address->getAddressLine2(),
      "City" => $billing_address->getLocality(),
      "State" => $billing_address->getAdministrativeArea(),
      "PostCode" => $billing_address->getPostalCode(),
      "Country" => $billing_address->getCountryCode(),
      "PhoneNumber" => "",
      "Email" => $owner->getEmail(),
    ];

    $identification = array(

    );

    $result = \Drupal::service('chargelogic.payment.checking')->CheckPayment($demandDepositAccount, $transaction, $billingaddress, $identification);


    return $result->CheckChargeResult;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {

  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $remote_id = $payment->getRemoteId();

    $price = $payment->getAmount()->getNumber();
    $curreny_code = $payment->getAmount()->getcurrencyCode();

    $options = new Options();
    $options->setApiKey($this->configuration['api_key']);
    $options->setSecretKey($this->configuration['secret_key']);
    $options->setBaseUrl($this->configuration['api_url']);

    $request = new CreateRefundRequest();
    $request->setPaymentTransactionId($remote_id);
    $request->setPrice($price);
    $request->setCurrency($curreny_code);
    $response = Refund::create($request, $options);

    if ($response->getStatus() == 'success') {
      $old_refunded_amount = $payment->getRefundedAmount();
      $new_refunded_amount = $old_refunded_amount->add($amount);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the CheckingMethodAddForm form elements. They are expected to be valid.
      'account_number', 'routing_number',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_method->account_number = $payment_details['account_number'];
    $payment_method->routing_number = $payment_details['routing_number'];

    $payment_method->setReusable(false);

    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {

  }

}
