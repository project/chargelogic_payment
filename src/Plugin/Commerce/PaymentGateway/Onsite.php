<?php

namespace Drupal\chargelogic_payment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;



/**
 * Provides the On-site payment gateway.
 *
 * @todo Allow customisation of allowed credit card types.
 *
 * @CommercePaymentGateway(
 *   id = "chargelogic_redirect_checkout",
 *   label = "Chargelogic",
 *   display_label = "Charelogic pay",
 *   forms = {
 *     "add-payment-method" = "Drupal\chargelogic_payment\PluginForm\PaymentMethodAddForm",
 *     "refund-payment" = "Drupal\chargelogic_payment\PluginForm\ChargeLogicPaymentRefundForm",
 *   },
 *   payment_method_types = {"chargelogic_credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class Onsite extends OnsitePaymentGatewayBase implements OnsiteInterface, SupportsRefundsInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration();
  }

  
  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $response = $this->makePayment($payment);


    $expires = 300;
    $next_state = 'authorization';

    if (!empty($response->ErrorMessage)) {
          \Drupal::logger('chargelogic_payment')->error($response->ErrorMessage);
          \Drupal::logger('chargelogic_payment')->notice('<pre>' . print_r($response, TRUE) . '</pre>');
          \Drupal::messenger()->addMessage($this->t('The payment has failed ').$response->ErrorMessage, 'error');
          throw new PaymentGatewayException($response->ErrorMessage);
    }

    if (!empty($response->AddressVerificationAlert)) {
      \Drupal::logger('chargelogic_payment')->error($response->AddressVerificationAlert);
      \Drupal::logger('chargelogic_payment')->notice('<pre>' . print_r($response, TRUE) . '</pre>');
      \Drupal::messenger()->addMessage($this->t('The payment has failed ').$response->AddressVerificationAlert, 'error');
      throw new PaymentGatewayException($response->AddressVerificationAlert);
     }

    if (!empty($response->CardVerificationValueAlert)) {
          \Drupal::logger('chargelogic_payment')->notice('<pre>' . print_r($response, TRUE) . '</pre>');
          \Drupal::logger('chargelogic_payment')->error($this->t('The payment has failed due to card ').$response->CardVerificationValueAlert);
          \Drupal::messenger()->addMessage($this->t('The payment has failed due to card ').$response->CardVerificationValueAlert, 'error');
          throw new HardDeclineException($response->CardVerificationValueAlert);
    }

    if ($response->TransactionStatus == "Approved" && $capture) {
      $next_state = 'completed';
    }

      $payment->setExpiresTime($expires);
      $payment->setState($next_state);
      $remote_id = $response->return_value;
      $payment->setRemoteId($remote_id);
      \Drupal::logger('chargelogic_payment')->notice('<pre>' . print_r($response, TRUE) . '</pre>');
      $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $response = $this->makePayment($payment);
    if ($response->getStatus() == "success") {
      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();
    }
  }

  /**
   * Prepare the request and send it to the gateway.
   */
  public function makePayment(PaymentInterface $payment, $capture = TRUE) {
    $order = $payment->getOrder();
    $payment_method = $payment->getPaymentMethod();

    $billing_address = $payment_method->getBillingProfile()->get('address')->first();

    $owner = $payment_method->getOwner();
    $buyer_id = sprintf('%05d', $owner->id());
   
    $encryption_service = \Drupal::service('encryption');
    // See if the card is turkish or not.
  
    $price = $payment->getAmount()->getNumber();
    $amount =  $payment->getAmount();
    $curreny_code = $payment->getAmount()->getcurrencyCode();
    $convert_to_try = FALSE;

    $card = array(
      "CardholderName" => $encryption_service->decrypt($payment_method->encrypted_full_holder_name->value),
      "AccountNumber" =>  $encryption_service->decrypt($payment_method->encrypted_full_card_number->value),
      "ExpirationMonth" => $encryption_service->decrypt($payment_method->encrypted_full_card_exp_month->value),
      "ExpirationYear" => $encryption_service->decrypt($payment_method->encrypted_full_card_exp_year->value),
      "CardVerificationValue" => $encryption_service->decrypt($payment_method->encrypted_full_card_cvv->value),
    );

    $transaction = array(
        "Amount" =>  $price,
        "ExternalReferenceNumber" => $order->id(),
        "Currency" => $curreny_code,
        "ConfirmationID" => $order->uuid(),
    );

    $billingaddress = array(
      "Name" => substr($billing_address->getAddressLine1() . ' ' . $billing_address->getAddressLine2(), 0, 60),
      "StreetAddress" => $billing_address->getAddressLine1(),
      "StreetAddress2" => $billing_address->getAddressLine2(),
      "City" =>  $billing_address->getLocality(),
      "State" => $billing_address->getAdministrativeArea(),
      "PostCode" => $billing_address->getPostalCode(),
      "Country" => $billing_address->getCountryCode(),
      "PhoneNumber" => "",
      "Email" => $owner->getEmail()
    );

    $result =  \Drupal::service('chargelogic.payment')->MakePayment($card, $transaction, $billingaddress);

    return $result->CreditCardAuthorizeResult;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {

  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
      $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
      // If not specified, refund the entire amount.
      $amount = $amount ?: $payment->getAmount();
      $this->assertRefundAmount($payment, $amount);

      // Perform the refund request here, throw an exception if it fails.


        $remote_id = $payment->getRemoteId();
        $decimal_amount = $amount->getNumber();
        $result =  \Drupal::service('chargelogic.payment')->MakeReversePayment($remote_id, $decimal_amount);
        $response = $result->CreditCardReverseResult;


      if (!empty($response->ErrorMessage)) {
          \Drupal::logger('chargelogic_payment')->error($response->ErrorMessage);
          \Drupal::messenger()->addMessage($this->t('The payment has failed ').$response->ErrorMessage, 'error');
          throw new PaymentGatewayException($response->ErrorMessage);
      }

      if (!empty($response->CardVerificationValueAlert)) {
          \Drupal::messenger()->addMessage($this->t('The payment has failed due to card ').$response->CardVerificationValueAlert, 'error');
          throw new HardDeclineException($response->CardVerificationValueAlert);
      }


      if($result->TransactionStatus == "Approved") {
          // Determine whether payment has been fully or partially refunded.
          $old_refunded_amount = $payment->getRefundedAmount();
          $new_refunded_amount = $old_refunded_amount->add($amount);
          if ($new_refunded_amount->lessThan($payment->getAmount())) {
              $payment->setState('partially_refunded');
          } else {
              $payment->setState('refunded');
          }

          $payment->setRefundedAmount($new_refunded_amount);
          $payment->save();
      }
      else {
          return false;
      }

  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'type', 'number', 'expiration',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_method->encrypted_full_holder_name = $payment_details['holder_name'];
    $payment_method->encrypted_full_card_type = $payment_details['type'];
    $payment_method->encrypted_full_card_number = $payment_details['number'];
    $payment_method->encrypted_full_card_exp_month = $payment_details['expiration']['month'];
    $payment_method->encrypted_full_card_exp_year = $payment_details['expiration']['year'];
    $payment_method->encrypted_full_card_cvv = $payment_details['security_code'];

    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
    $payment_method->setReusable(false);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {

  }

}
