<?php

namespace Drupal\chargelogic_payment\Plugin\Commerce\PaymentMethodType;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the checking payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "chargelogic_checking_method",
 *   label = @Translation("Chargelogic checking"),
 *   create_label = @Translation("Chargelogic checking"),
 * )
 */
class CheckingPayment extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {

  $account_number = $payment_method->account_number->value;
  $routing_number = $payment_method->routing_number->value;



  $args = [
      '@routing_number' => substr($routing_number, -4),
      '@account_number' => substr($account_number, -4),
  ];
    return $this->t('Account number ending in @account_number and Routing number ending in @routing_number' , $args);

  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['account_number'] = BundleFieldDefinition::create('string_long')
      ->setLabel($this->t('Account Number'))
      ->setDescription($this->t('Account Number'))
      ->setRequired(TRUE);


  $fields['financial_institution'] = BundleFieldDefinition::create('list_integer')
      ->setLabel(t('Financial Institution'))
      ->setRequired(TRUE)
      ->setSettings(['allowed_values_function' => 'financial_institution_values_function'])
      ->setDefaultValue(3);


    $fields['routing_number'] = BundleFieldDefinition::create('string_long')
      ->setLabel($this->t('Routing Number'))
      ->setDescription($this->t('Routing Number'))
      ->setRequired(TRUE);

    return $fields;
  }

}
