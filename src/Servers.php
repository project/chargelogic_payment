<?php
/*
 * ChargeLogicConnect
 *
 * This file was automatically generated for ChargeLogic Connect by APIMATIC v2.0 ( https://apimatic.io ).
 */

namespace Drupal\chargelogic_payment;

/**
 * Baseurl aliases
 */
class Servers
{
    /**
     * TODO: Write general description for this element
     */
    const CHARGELOGIC_CONNECT_REST_ENDPOINT = "ChargeLogic Connect REST Endpoint";
}
