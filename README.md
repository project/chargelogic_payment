CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Features
 * Maintainers
 * Testing instruction
 * Encryption Key

INTRODUCTION
------------

This module integrates https://www.chargelogic.com with
https://www.drupal.org/project/commerce providing a credit card payment
gateway. You can add an On-Site credit card or checking payment gateway to your
Drupal Commerce shop.chargelogic_payment supports amex, dinersclub, discover,
jcb, maestro, mastercard, visa, troy and visa electron types.


REQUIREMENTS
------------

 * Drupal Commerce
 * Encryption


INSTALLATION
------------

 * The credit card data is encrypted by the
   https://www.drupal.org/project/encryption module. Therefore you need to
   install the module first and then generate an encryption key and finally put
   it on the settings.php file. The details are explained on the encryption
   module project page.


CONFIGURATION
-------------

 * After the installation, go to `admin/commerce/config/payment-gateways` and
   add a payment gateway. Choose chargelogic and add your credentials.
 * That's it, now you should be able to see the credit card payment screen on
   the payment page.



FEATURES
--------

 * Encrypt credit card information completely using the
   https://www.drupal.org/project/encryption module.



MAINTAINERS
-----------

Current maintainers:
 * Himmat Bhatia - https://www.drupal.org/u/himmatbhatia



TESTING INSTRUCTIONS
--------------------

 * You can see test credit cards on below
   VISA: 4111 1111 1111 1111   - 111 or 123 or 545
   Discover: 6011111111111117  - 111 or 123
   Mastercard: 5555555555554444  545 or 123
   American Express: 3782 8224 6310 005 1234
   Try a few credit cards to see if the gateway is working properly.


ENCRYPTION KEY
--------------

The encryption key should go in your settings.php file as a base 64 encoded 256
bit value. A random value can be generated with the following command in Linux.

dd bs=1 count=32 if=/dev/urandom | openssl base64

ex-$settings['encryption_key'] = 'IPMj1A1H5w+EMrN5a+w3Y8MUv0CsAAPM5OfaGwMOou4=';


**This payment gateway requires cURL.**
